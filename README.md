# Misfortune

## What is this?
This repository contains the bits you need to turn [The Endless Doomscroller](https://endlessdoomscroller.com/) into a [fortune](https://linux.die.net/man/6/fortune) database. The code is a first iteration in Ruby as an exercise to better learn the language. 

[![asciicast](https://asciinema.org/a/351692.svg)](https://asciinema.org/a/351692)


## Inspirations
  - [The Endless Doomscroller](https://endlessdoomscroller.com/)
