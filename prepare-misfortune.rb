#!/usr/bin/env ruby

def download_doom_scroll()
    puts("Let us download https://endlessdoomscroller.com/script.js")
    require 'open-uri'
    download = open('https://endlessdoomscroller.com/script.js')
    IO.copy_stream(download, 'script.js')
end

def parse_doom_scroll()
    misfortunes = []
    puts("Let's look for things in script.js that start with '\"'.")
    puts("I am going to parse script.js.")
    file='script.js'
    f = File.open(file, "r")
    f.each_line { |line|
    #   puts line
      if line.match(/^"/)
        no_comma = line.gsub(',', '')
        # omg = line
        # puts("I did a chomp.")
        # omg = line.gsub(line, ',')
        # puts omg
        puts no_comma
        misfortunes.push(no_comma)
      end
    }
    f.close
    puts "Wow! We found #{misfortunes.length()} misfortunes to share.\n\n"
    return misfortunes
end

def make_dat_file(misfortunes)
    File.open("misfortunes", "w+") do |f| 
        misfortunes.each { |x| f.puts "#{x.gsub(',', '')}%"}
    end
end

puts("Preparing your misfortune.")
puts("--------------------------")

## Save script.js
download_doom_scroll()

## Parse out the fortunes
our_misfortunes = parse_doom_scroll()

## Assemble a .dat file for use with fortune
make_dat_file(our_misfortunes)

## Fin.
puts("Next Steps:\n\n  - Run 'strfile misfortunes' \n  - Test by running 'fortune misfortunes' from this directory. \n  - If it all works, run 'sudo cp misfortunes* /usr/share/games/fortune/' on Ubuntu systems. Your mileage may vary elsewhere.")
